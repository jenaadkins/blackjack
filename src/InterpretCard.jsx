export const interpretCard = number => {
  let value = (number % 13) + 1;
  let temp = Math.floor(number / 13);
  let faceValue = "";
  let suite = "";

  if (temp === 0) {
    temp = "Hearts";
  } else if (temp === 1) {
    suite = "Diamonds";
  } else if (temp === 3) {
    suite = "Spades";
  } else if (temp === 4) {
    suite = "Clubs";
  }
  switch (value) {
    case 13: {
      faceValue = "K";
      value = 10;
      break;
    }
    case 12: {
      faceValue = "Q";
      value = 10;
      break;
    }
    case 11: {
      faceValue = "J";
      value = 10;
      break;
    }
    case 1: {
      faceValue = "A";
      value = 1;
      break;
    }

    default: {
      faceValue = JSON.stringify(value);
    }
  }

  if (value - 10 > 0) {
    value = 10;
  }
  return { value: value, suite: suite, faceValue: faceValue };
};
