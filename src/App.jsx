import logo from "./logo.svg";
import "./App.scss";
import { useReducer } from "react";
import { reducer, initialState } from "./GlobalContext/reducer";
import { GlobalContext } from "./GlobalContext/GlobalContext";
import { Header } from "./components/Header";
import { CardSpace } from "./components/CardSpace";
import { BtnContainer } from "./components/BtnContainer";

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <GlobalContext.Provider value={{ state: state, dispatch: dispatch }}>
      <div className="container">
        <Header />
        <div className="card-container">
          <CardSpace />
        </div>
        <BtnContainer />
      </div>
    </GlobalContext.Provider>
  );
}

export default App;
