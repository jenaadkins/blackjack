import { useContext } from "react";
import { GlobalContext } from "./GlobalContext";

export const useGlobal = () => {
  let globalContext = useContext(GlobalContext);
  const aa = (type, payload) => {
    globalContext.dispatch({ type, payload });
  };

  return [globalContext, aa];
};
