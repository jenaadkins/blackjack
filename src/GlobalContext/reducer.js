export const ADD_PLAYER_CARD = "ADD_PLAYER_CARD";
export const ADD_DEALER_CARD = "ADD_DEALER_CARD";
export const SET_PLAYER_DONE = "SET_PLAYER_DONE";
export const SET_DEALER_DONE = "SET_DEALER_DONE";
export const NEW_HAND = "NEW_HAND";

export const reducer = (state, action) => {
  let newState = JSON.parse(JSON.stringify(state));

  const { type, payload } = action;

  switch (type) {
    case "ADD_PLAYER_CARD": {
      newState.playerHand.push(payload);
      newState.dealtCards.push(payload);
      return newState;
    }
    case "ADD_DEALER_CARD": {
      newState.dealerHand.push(payload);
      newState.dealtCards.push(payload);
      return newState;
    }
    case "SET_PLAYER_DONE": {
      newState.isPlayerDone = true;
      return newState;
    }
    case "SET_DEALER_DONE": {
      newState.isDealerDone = true;
      return newState;
    }
    case "NEW_HAND": {
      newState.isPlayerDone = false;
      newState.isDealerDone = false;
      newState.playerHand = [];
      newState.dealerHand = [];
      newState.dealtCards = [];
      return newState;
    }

    default:
      return newState;
  }
};

export const initialState = {
  playerHand: [],
  dealerHand: [],
  dealtCards: [],
  settings: {},
  winsLosses: {
    wins: 0,
    losses: 0
  },
  wallet: 100,
  currentBet: 0,
  minBet: 1,
  maxBet: 100,
  isPlayerDone: false,
  isDealerDone: false
};

export const aa = (type, payload) => {
  return { type, payload };
};
