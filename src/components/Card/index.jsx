//{ value: value, suite: suite, faceValue: faceValue }
import "./Card.scss";
import Heart from "../../assets/cards-heart.svg";
import Diamond from "../../assets/cards-diamond.svg";
import Spade from "../../assets/cards-spade.svg";
import Club from "../../assets/cards-club.svg";

export const Card = props => {
  const { value, suite, faceValue } = props;

  const getSuitImage = props => {
    switch (suite) {
      case "Hearts": {
        return Heart;
      }
      case "Diamond": {
        return Diamond;
      }
      case "Spade": {
        return Spade;
      }
      case "Club": {
        return Club;
      }
      default: {
        return "";
      }
    }
  };
  return (
    <div className="Card">
      <div className="CardValue">
        <span>{faceValue}</span>
        <img src={getSuitImage()} />
      </div>
    </div>
  );
};
