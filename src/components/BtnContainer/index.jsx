import { GlobalContext } from "../../GlobalContext/GlobalContext";
import { useContext, useState } from "react";
import {
  ADD_PLAYER_CARD,
  ADD_DEALER_CARD,
  SET_PLAYER_DONE,
  SET_DEALER_DONE,
  NEW_HAND,
  aa
} from "../../GlobalContext/reducer";
import { useGlobal } from "../../GlobalContext/useGlobal";
import { InterpretCard, interpretCard } from "../../InterpretCard";
export const BtnContainer = props => {
  const [globalContext, aa] = useGlobal();

  const generateDeck = maxIndex => {
    let array = [];
    for (let temp_1 = 0; temp_1 < maxIndex; temp_1++) {
      array.push(temp_1);
    }
    return array;
  };

  const [deck, setDeck] = useState(generateDeck());

  const getRandomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  const drawCard = (isPlayer, genDeck = false) => {
    let currentDeck = deck.length === 0 || genDeck ? generateDeck(52) : deck;
    const random_index = getRandomNumber(0, currentDeck.length - 1);
    let drawnCard = currentDeck.splice(random_index, 1);
    console.log("thecard", interpretCard(drawnCard));
    console.log(deck);
    if (isPlayer) {
      aa(ADD_PLAYER_CARD, drawnCard);
      console.log("playhand", globalContext.playerHand);
    } else {
      aa(ADD_DEALER_CARD, drawnCard);

      console.log("dealhand", globalContext.playerHand);
    }
    setDeck(currentDeck);
    console.log("card", drawnCard);
  };

  const onStand = isPlayer => {
    if (isPlayer) {
      aa(SET_PLAYER_DONE);
      console.log("a", globalContext);
    } else {
      aa(SET_DEALER_DONE);
      console.log("b", globalContext);
    }
  };

  const nextHand = () => {
    if (globalContext.state.isPlayerDone && globalContext.state.isDealerDone) {
      return false;
    } else {
      return true;
    }
  };

  const goNextHand = () => {
    aa(NEW_HAND);
    let newDeck = generateDeck(52);
    drawCard(true, true);
    drawCard(true);
    drawCard(false);
    drawCard(false);

    console.log(globalContext);
  };

  return (
    <div className="btn-container">
      <div className="action-container">
        <button
          onClick={() => {
            drawCard(true);
          }}
          disabled={globalContext.state.isPlayerDone}
        >
          Hit
        </button>
        <button
          onClick={() => {
            onStand(true);
            onStand(false);
          }}
          disabled={globalContext.state.isPlayerDone}
        >
          Stand
        </button>
        <button
          onClick={() => {
            goNextHand();
          }}
          disabled={nextHand()}
        >
          Next Hand
        </button>
      </div>
      <div className="bet-container"></div>
    </div>
  );
};
