import { GlobalContext } from "../../GlobalContext/GlobalContext";
import { useContext } from "react";

export const Header = props => {
  let globalContext = useContext(GlobalContext);
  console.log(globalContext);
  return (
    <header>
      <button>Play</button>
      <button>Rules</button>
      <button>Settings</button>
    </header>
  );
};
